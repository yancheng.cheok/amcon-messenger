package org.yccheok.amconmessenger;

/**
 * Created by yccheok on 29/1/2016.
 */
public class Sms {
    public Sms(String phone, String message, long timestamp) {
        this.phone = phone;
        this.message = message;
        this.timestamp = timestamp;
    }

    public final String phone;
    public final String message;
    public final long timestamp;
}
