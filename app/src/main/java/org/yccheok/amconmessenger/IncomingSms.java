package org.yccheok.amconmessenger;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import com.google.gson.Gson;

/**
 * Created by yccheok on 29/1/2016.
 */
public class IncomingSms extends BroadcastReceiver {
    public void onReceive(Context context, Intent intent) {

        // Retrieves a map of extended data from the intent.
        final Bundle bundle = intent.getExtras();

        try {

            if (bundle != null) {

                final Object[] pdusObj = (Object[]) bundle.get("pdus");

                for (int i = 0; i < pdusObj.length; i++) {

                    SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) pdusObj[i]);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();

                    String senderNum = phoneNumber;
                    String message = currentMessage.getDisplayMessageBody();
                    long timestamp = currentMessage.getTimestampMillis();

                    Sms sms = new Sms(senderNum.trim(), message.trim(), timestamp);

                    Gson gson = new Gson();
                    String smsString = gson.toJson(sms);
                    AmconApplication.instance().getSharedPreferences().edit().putString("SMS", smsString).commit();

                    BusProvider.getInstance().post(sms);
                } // end for loop
            } // bundle is null

        } catch (Exception e) {
            Log.e("SmsReceiver", "Exception smsReceiver" + e);
        }
    }

    // Get the object of SmsManager
    private final SmsManager sms = SmsManager.getDefault();
}
