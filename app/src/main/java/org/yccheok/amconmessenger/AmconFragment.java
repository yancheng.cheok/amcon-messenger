package org.yccheok.amconmessenger;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yccheok on 28/1/2016.
 */
public class AmconFragment extends Fragment {
    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onDestroy () {
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.amcon_fragment, container, false);

        Utils.setCustomTypeFace(view.findViewById(R.id.empty), Utils.ROBOTO_LIGHT_TYPE_FACE);
        Utils.setCustomTypeFace(view.findViewById(R.id.info), Utils.ROBOTO_MEDIUM_TYPE_FACE);
        Utils.setCustomTypeFace(view.findViewById(R.id.get_info_button), Utils.ROBOTO_MEDIUM_TYPE_FACE);

        Utils.setCustomTypeFace(view.findViewById(R.id.date_time), Utils.ROBOTO_REGULAR_TYPE_FACE);

        final Button button = (Button)view.findViewById(R.id.get_info_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phoneNumber = Utils.getPhoneNumber();
                if (phoneNumber == null || phoneNumber.trim().isEmpty()) {
                    showSnackBar(getString(R.string.phone_number_required), R.color.button_text_color, true);
                    return;
                }

                phoneNumber = phoneNumber.trim();

                button.setEnabled(false);
                sendSMS(phoneNumber, "get info");
            }
        });

        String smsString = AmconApplication.instance().getSharedPreferences().getString("SMS", null);
        if (smsString != null) {
            Gson gson = new Gson();
            try {
                Sms sms = gson.fromJson(smsString, Sms.class);
                if (sms != null) {
                    this.receive(view, sms);
                }
            } catch (Exception e) {
            }
        }

        return view;
    }

    private void sendSMS(String phoneNumber, String message)
    {
        String SENT = "SMS_SENT";

        PendingIntent sentPI = PendingIntent.getBroadcast(this.getActivity(), 0,
                new Intent(SENT), 0);

        //---when the SMS has been sent---
        this.getActivity().registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        showSnackBar(getString(R.string.sms_success), R.color.button_text_color);
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        showSnackBar(String.format(getString(R.string.sms_fail_template), "Generic failure"), R.color.pastel_red);
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        showSnackBar(String.format(getString(R.string.sms_fail_template), "No service"), R.color.pastel_red);
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        showSnackBar(String.format(getString(R.string.sms_fail_template), "Null PDU"), R.color.pastel_red);
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        showSnackBar(String.format(getString(R.string.sms_fail_template), "Radio off"), R.color.pastel_red);
                        break;
                }

                View view = getView();
                view.findViewById(R.id.get_info_button).setEnabled(true);
            }
        }, new IntentFilter(SENT));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, null);
    }

    private void showSnackBar(String message, int colorResId) {
        showSnackBar(message, colorResId, false);
    }

    private void showSnackBar(String message, int colorResId, boolean action) {
        Snackbar snackbar = Snackbar.make(this.getView(), message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(getResources().getColor(colorResId));

        if (action) {
            snackbar.setActionTextColor(getResources().getColor(R.color.pastel_red));
            snackbar.setAction(getString(R.string.menu_settings), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Activity activity = getActivity();
                    if (activity instanceof AmconFragmentActivity) {
                        ((AmconFragmentActivity)activity).settings();
                    }
                }
            });
        }
        snackbar.show();
    }

    public void parse(View view, String message, long timestamp)
    {
        String location = "";
        List<Pair<String, String>> infos = new ArrayList<>();
        String lines[] = message.split("\\r?\\n");

        // Create a Pattern object
        String pattern = "===(.+)===";
        Pattern r = Pattern.compile(pattern);

        for (String line : lines) {
            // Now create matcher object.
            Matcher m = r.matcher(line);

            if (m.find()) {
                if (m.groupCount() >= 1) {
                    location = m.group(1).trim();
                }
            }

            String[] v = line.split(":");
            if (v.length == 2) {
                infos.add(Pair.create(v[0], v[1]));
            }
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy h:mm a");
        String dateTime = sdf.format(timestamp);
        updateUI(view, location, dateTime, infos);
    }

    private void updateUI(final View view, final String location, final String dateTime, final List<Pair<String, String>> infos) {
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                _updateUI(view, location, dateTime, infos);
            }
        });
    }

    private void _updateUI(final View view, String location, String dateTime, List<Pair<String, String>> infos) {
        view.findViewById(R.id.empty).setVisibility(View.GONE);
        view.findViewById(R.id.info).setVisibility(View.VISIBLE);
        ((TextView)view.findViewById(R.id.location)).setText(location);
        ((TextView)view.findViewById(R.id.date_time)).setText(dateTime);

        LinearLayout container = (LinearLayout)view.findViewById(R.id.container);
        container.removeAllViews();

        int i = 0;
        for (Pair<String, String> pair : infos) {
            View v = LayoutInflater.from(this.getActivity()).inflate(R.layout.info_row, container, false);
            TextView label = (TextView)v.findViewById(R.id.label);
            TextView value = (TextView)v.findViewById(R.id.value);
            Utils.setCustomTypeFace(label, Utils.ROBOTO_LIGHT_TYPE_FACE);
            Utils.setCustomTypeFace(value, Utils.ROBOTO_LIGHT_TYPE_FACE);
            label.setText(pair.first);
            value.setText(pair.second);
            if (((i++) & 1) == 1) {
                v.setBackgroundColor(getResources().getColor(R.color.darker_background));
            } else {
                v.setBackgroundColor(getResources().getColor(R.color.lighter_background));
            }
            container.addView(v);
        }
    }

    @Subscribe
    public void receive(Sms sms) {
        receive(this.getView(), sms);
    }

    public void receive(View view, Sms sms) {
        if (sms.phone.endsWith(Utils.getPhoneNumber())) {
            parse(view, sms.message, sms.timestamp);
        }
    }
}
