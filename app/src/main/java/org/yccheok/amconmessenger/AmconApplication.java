package org.yccheok.amconmessenger;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by yccheok on 27/1/2016.
 */
public class AmconApplication extends Application {
    private static AmconApplication me;
    private SharedPreferences sharedPreferences;

    public void onCreate() {
        super.onCreate();
        me = this;
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public static AmconApplication instance() {
        return me;
    }
}
