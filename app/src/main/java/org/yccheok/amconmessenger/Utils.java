package org.yccheok.amconmessenger;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by yccheok on 27/1/2016.
 */
public class Utils {
    public static void setCustomTypeFace(View view, Typeface typeFace) {
        if (view instanceof TextView) {
            ((TextView)view).setTypeface(typeFace);
        } else if (view instanceof EditText) {
            ((EditText)view).setTypeface(typeFace);
        } else if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup)view;
            int count = viewGroup.getChildCount();
            for (int i = 0; i < count; i++) {
                setCustomTypeFace(viewGroup.getChildAt(i), typeFace);
            }
        }
    }

    public static String getPhoneNumber() {
        return AmconApplication.instance().getSharedPreferences().getString("PHONE", null);
    }

    public static final Typeface ROBOTO_BOLD_TYPE_FACE = Typeface.createFromAsset(AmconApplication.instance().getAssets(), "fonts/Roboto-Bold.ttf");
    public static final Typeface ROBOTO_LIGHT_TYPE_FACE = Typeface.createFromAsset(AmconApplication.instance().getAssets(), "fonts/Roboto-Light.ttf");
    public static final Typeface ROBOTO_REGULAR_TYPE_FACE = Typeface.createFromAsset(AmconApplication.instance().getAssets(), "fonts/Roboto-Regular.ttf");
    public static final Typeface ROBOTO_MEDIUM_TYPE_FACE = Typeface.createFromAsset(AmconApplication.instance().getAssets(), "fonts/Roboto-Medium.ttf");
}
