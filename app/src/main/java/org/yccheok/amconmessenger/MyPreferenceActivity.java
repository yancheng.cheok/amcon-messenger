package org.yccheok.amconmessenger;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

/**
 * Created by cheok on 04/02/2016.
 */
public class MyPreferenceActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.preference_activity);

        if (savedInstanceState == null) {
            // Display the fragment as the main content.
            MyPreferenceFragment myPreferenceFragment = new MyPreferenceFragment();
            this.getSupportFragmentManager().beginTransaction().replace(R.id.content, myPreferenceFragment).commit();
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            this.finish();
        }
        return true;
    }
}
